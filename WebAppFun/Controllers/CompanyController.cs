﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppFun.Models;

namespace WebAppFun.Controllers
{
    public class CompanyController : Controller
    {
        // GET: Company
        public ActionResult Index()
        {
            //create a dbhandler
            //call the get dbhandler
            //add this result to the view.
            LeadDBHandler leadDBHandler = new LeadDBHandler();
            return View(leadDBHandler.GetCompanyItemList());
        }





        // Add company (get) so shows the form
        [HttpGet]
        public ActionResult Create()
        {

            return View();
        }

        //Add company (post) so shows form and actually inserts the lead from post data
        [HttpPost]
        public ActionResult Create(Company company)
        {
            if (ModelState.IsValid)
            {
                LeadDBHandler leadDBHandler = new LeadDBHandler();
                if (leadDBHandler.InsertCompany(company))
                {
                    ViewBag.Message = "Company Added Successfully";
                    ModelState.Clear();
                }
            }
            return View();
        }








    }




}