﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using WebAppFun.Models;

namespace WebAppFun.Controllers
{
    public class QueryController : Controller
    {
        
        

        //Handle the sql slection (posted from form)
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        public ActionResult Select()
        {
            
            LeadDBHandler leadDBHandler = new LeadDBHandler();
            if (Request.HttpMethod == "POST") {

                //validate the query so it always starts with a select statement. We only want to do select statements
                //These statements start with SELECT therefore only allow queries that start with this statement
                Regex regex = new Regex(@"^\s*SELECT\s", RegexOptions.IgnoreCase );
                if (regex.IsMatch(Request["query"]))
                {
                    ViewData["query_result"] = leadDBHandler.QueryDB(Request["query"]);
                }
                else
                {
                    ViewData["query_result"] = "ERROR: The query must be a select statement";
                }
                
                
            } 
            
                return View();

        }
    }
}