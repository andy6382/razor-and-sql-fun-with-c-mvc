﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppFun.Models;

namespace WebAppFun.Controllers
{
    public class LeadController : Controller
    {
        // GET: Lead
        public ActionResult Index()
        {
            LeadDBHandler leadDBHandler = new LeadDBHandler();
            return View(leadDBHandler.GetItemList());
        }

        // Add lead (get) so shows the form
        [HttpGet]
        public ActionResult Create()
        {

            return View();
        }

        //Add lead (post) so shows form and actually inserts the lead from post data
        [HttpPost]
        public ActionResult Create(Lead lead)
        {
            if (ModelState.IsValid)
            {
                LeadDBHandler leadDBHandler = new LeadDBHandler();
                if (leadDBHandler.InsertItem(lead))
                {
                    ViewBag.Message = "Lead Added Successfully";
                    ModelState.Clear();
                }
            }
            return View();
        }
    }
}
        