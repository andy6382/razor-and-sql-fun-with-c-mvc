﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppFun.Models
{
    public class Company
    {
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public double Revenue { get; set; }
    }
}