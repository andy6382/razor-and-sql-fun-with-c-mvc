﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebAppFun.Models
{
    public class LeadDBHandler
    {
        private SqlConnection con;
        private void connection()
        {
            string constring = ConfigurationManager.ConnectionStrings["WebFunConString"].ToString();
            con = new SqlConnection(constring);
        }

        

        // 1. ********** Insert Item **********
        public bool InsertItem(Lead iList)
        {
            connection();
            string query = "INSERT INTO Lead VALUES('" + iList.Name + "','" + iList.Employer + "','" + iList.PhoneNumber + "'," + iList.Age + ")";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        public bool InsertCompany(Company iList)
        {
            connection();
            string query = "INSERT INTO Company VALUES('" + iList.CompanyName + "'," + iList.Revenue + ")";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        // 2. ********** Get All Item List **********
        public List<Lead> GetItemList()
        {
            connection();
            List<Lead> iList = new List<Lead>();

            string query = "SELECT * FROM Lead";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();

            con.Open();
            adapter.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                iList.Add(new Lead
                {
                    ID = Convert.ToInt32(dr["ID"]),
                    Name = Convert.ToString(dr["Name"]),
                    PhoneNumber = Convert.ToString(dr["PhoneNumber"]),
                    Employer = Convert.ToString(dr["Employer"]),
                    Age = Convert.ToInt32(dr["Age"])
                });
            }
            return iList;
        }

        
        public List<Company> GetCompanyItemList()
        {
            connection();
            List<Company> iList = new List<Company>();

            string query = "SELECT * FROM Company";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();

            con.Open();
            adapter.Fill(dt);
            con.Close();

            foreach (DataRow dr in dt.Rows)
            {
                iList.Add(new Company
                {
                    CompanyID = Convert.ToInt32(dr["CompanyID"]),
                    CompanyName = Convert.ToString(dr["CompanyName"]),
                    Revenue = Convert.ToDouble(dr["Revenue"])
                });
            }
            return iList;
        }



        // 3. ********** Any Query **********
        public string QueryDB(String dbquery)
        {
            string result = "";
            connection();
            SqlCommand cmd = new SqlCommand(dbquery, con);

            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++) {
                        result += reader[i].ToString() + "\t";
                    }
                    result += "\n";
                }
                

            }
            catch(SqlException ex)
            {
                return "Ooops... the query failed:\n\n" + ex.ToString();
            }
            finally
            {
                con.Close();
            }

            return result;

            
        }

        //// 3. ********** Update Item Details **********
        //public bool UpdateItem(Lead iList)
        //{
        //    connection();
        //    string query = "UPDATE Lead SET Name = '" + iList.Name + "', Category = '" + iList.Category + "', Price = " + iList.Price + " WHERE ID = " + iList.ID;
        //    SqlCommand cmd = new SqlCommand(query, con);
        //    con.Open();
        //    int i = cmd.ExecuteNonQuery();
        //    con.Close();

        //    if (i >= 1)
        //        return true;
        //    else
        //        return false;
        //}

        //// 4. ********** Delete Item **********
        //public bool DeleteItem(int id)
        //{
        //    connection();
        //    string query = "DELETE FROM Lead WHERE ID = " + id;
        //    SqlCommand cmd = new SqlCommand(query, con);
        //    con.Open();
        //    int i = cmd.ExecuteNonQuery();
        //    con.Close();

        //    if (i >= 1)
        //        return true;
        //    else
        //        return false;
        //}
    }
}

 




        
        